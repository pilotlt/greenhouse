'''despite of at the moment I do not use subscribe section of this code,  I left it for the future us'''
import time
import paho.mqtt.client as paho
broker="192.168.0.110" # input here IP address of your Domoticz server

#define callback
def on_message(client, userdata, message):
    time.sleep(1)
    print("received message =",str(message.payload.decode("utf-8"))) # recieved from ESP

client= paho.Client("client-001")

######Bind function to callback
client.on_message=on_message

print("connecting to broker ",broker)
client.connect(broker)# connect to broker
client.loop_start() # start loop to process received messages
print("subscribing ")
client.subscribe("hello")# subscribe
time.sleep(2)
print("publishing ")
# publish to topic irrig_on_off
client.publish("filling_on_off","on")# publish message
time.sleep(4)
client.disconnect() # disconnect
client.loop_stop() # stop loop
