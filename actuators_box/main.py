''' @Boris Vinikov www.verysecretlab.eu '''
#############################################

from machine import Pin
import time
from umqttsimple import MQTTClient
import ubinascii
import machine
import micropython
import network
import esp
esp.osdebug(None)
import gc
gc.collect()

############################################
try:

    button_1 = Pin(0, Pin.IN, Pin.PULL_UP)
    button_2 = Pin(2, Pin.IN, Pin.PULL_UP) 

    connect_led = Pin(12, Pin.OUT)
    irrig_led = Pin(13, Pin.OUT)
    wsupp_led = Pin(14, Pin.OUT)
    connect_led(1)
    irrig_led(1)
    wsupp_led(1)
    time.sleep(1)
    connect_led(0)
    irrig_led(0)
    wsupp_led(0)

    relay_1 = Pin(5, Pin.OUT)
    relay_2 = Pin(4, Pin.OUT) 

    #############################################
    # Set relays to low
    relay_1(0)
    relay_2(0)
    
    #############################################

    ssid = 'YOURSSID' # put your SSID here
    password = 'xxxxxxxx' # put password here

    #############################################

    mqtt_server = '192.168.0.110' # IP address of your mosquitto broker
    client_id = ubinascii.hexlify(machine.unique_id())
    print('client id',client_id)
    topic_sub = b'irrig_on_off'
    topic_pub = b'hello' # notr in use actually
    topic1_sub = b'filling_on_off'

    last_message = 0
    message_interval = 5
    counter = 0

    station = network.WLAN(network.STA_IF)

    def sub_cb(topic, msg):
      print((topic, msg))
      if topic == b'irrig_on_off' and msg == b'on':
        print('ESP received ON message')
        relay_1(1)
        irrig_led(1)
      if topic == b'irrig_on_off' and msg == b'off':
        print('ESP received OFF message')
        relay_1(0)
        irrig_led(0)

      if topic == b'filling_on_off' and msg == b'on':
        print('ESP received filling ON message')
        relay_2(1)
        wsupp_led(1)
      if topic == b'filling_on_off' and msg == b'off':
        print('ESP received filling OFF message')
        relay_2(0)
        wsupp_led(0)

    def connect_and_subscribe():
      global client_id, mqtt_server, topic_sub
      global client_id, mqtt_server, topic1_sub
      time.sleep(0.5)
      client = MQTTClient(client_id, mqtt_server)
      time.sleep(0.5)
      client.set_callback(sub_cb)
      time.sleep(0.5)
      client.connect()
      time.sleep(0.5)
      client.subscribe(topic_sub)
      client.subscribe(topic1_sub)
      time.sleep(0.5)
      connect_led(1)
      print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic_sub))
      print('Connected to %s MQTT broker, subscribed to %s topic' % (mqtt_server, topic1_sub))
      return client

    ####### connecting to network###########
    station.active(True)
    station.connect(ssid, password)

    while station.isconnected() == False:
      pass

    print('Connection successful')
    print(station.ifconfig())
    #######################################

    print('connecting MQTT')
    client = connect_and_subscribe()
    print('ID')
    print(client_id)

    while True:

        if not button_1.value():
            relay_1(1)
            irrig_led(1)
            print('irrigation on locally') # irrigation is switched on by hardware button
            time.sleep_ms(300)
            while not button_1.value():
                relay_1(0)
                irrig_led(0)
                print('irrigation off locally') # irrigation is switch off by hardware button
                time.sleep_ms(300)

        elif not button_2.value():
            relay_2(1)
            wsupp_led(1)
            print('water supply on locally') # water supply valve is switched on by hardware button
            time.sleep_ms(300)
            while not button_2.value():
                relay_2(0)
                wsupp_led(0)
                print('water supply off locally')  # water supply valve is switched off by hardware button
                time.sleep_ms(300)

        client.check_msg()
        if (time.time() - last_message) > message_interval:
          msg = b'Hello #%d' % counter
          client.publish(topic_pub, msg)
          last_message = time.time()
          counter += 1

except Exception as e:
    print('ERROR', e)
    for i in range(5):
        connect_led(0)
        time.sleep(0.5)
        connect_led(1)
    machine.reset()
