'''Greenhouse sensors by Boris Vinikov'''

import wifimgr
import dht
import time
import ssd1306
import machine
import urequests
import ds18x20
import onewire

try:

    i2c = machine.I2C(scl=machine.Pin(5), sda=machine.Pin(4))
    oled = ssd1306.SSD1306_I2C(128, 64, i2c)

    oled.fill(0)
    oled.text('Starting up...', 0, 0)
    oled.show()
    time.sleep(1)

    # ds18x20 is on GPIO12
    dat = machine.Pin(12)
    # create the onewire object
    ds = ds18x20.DS18X20(onewire.OneWire(dat))
    # scan for devices on the bus
    roms = ds.scan()

    # DHT sensor on GPIO00
    d=dht.DHT22(machine.Pin(0))

    # soil sensor on ADC
    adc = machine.ADC(0) # Pin to Read Soil sensor voltage


            ######################
            # Sensor calibration #
            ######################

            # values on right are inverse * 1000 values on left
            # dry air = 880 (0%) = 1.1363636363636363636363636363636
            # water = 483 (100%) = 2.0703933747412008281573498964803
            # The Difference     = 0.93402973837756446452098626011673
            # 1 %                = 0.00934029738377564464520986260117

    url_json = 'http://XXX.XXX.X.XXX:8080/json.htm?type=command&param=udevice&idx=' # put your IP of your local DOMOTICZ server instead of "Xs"
    sensor_idx  = 2 # put here idx number of DHT sensor
    ground_idx = 3 #idx number of DS18X20
    soil_idx = 4 # idx number of soil sensor

    wlan = wifimgr.get_connection()  # connecting to wlan
    print('connected')
    oled.fill(0)
    oled.text('Network - ok', 0, 30)
    oled.show()
    time.sleep(3)

    if wlan is None:
        print("Could not initialize the network connection.")
        oled.fill(0)
        oled.text('Network not ok!', 0, 30)
        oled.show()
        while True:
            pass

    while True:
        # taking measurments from DS18X20
        print('GR_temp:', end=' ')
        ds.convert_temp()
        for rom in roms:

            t_ground = str(round(ds.read_temp(rom),1))
            print(ds.read_temp(rom), end=' ')
            time.sleep(5)
        print()

        ################################################
        # taking measurments from Soil sensor
        SoilMoistVal = (((1 / adc.read())* 1000) / 0.00934029738377564464520986260117) - 122
        print(adc.read())
        if SoilMoistVal > 100:
            SoilMoistVal = 100
        if SoilMoistVal < 0:
            SoilMoistVal = 0
        print('Moisture=', SoilMoistVal)
        ################################################
        # taking measurments from DHT22
        d.measure()
        t_int = d.temperature()
        h_int = d.humidity()

        if h_int > 70:
            HUM_STAT = 3
        elif h_int > 30:
            HUM_STAT = 1
        else:
            HUM_STAT = 2

        oled.fill(0)
        oled.text('T-AIR = '+str(t_int)+'C', 0,20)
        oled.text('H_AIR. = '+str(h_int)+'%', 0,30)
        oled.text('T_GR. = '+str(t_ground)+'C', 0,40)
        oled.text('S_GR. = '+str(round(SoilMoistVal))+'CD', 0,50)
        oled.show()

        cmd = url_json  + str(sensor_idx) + '&nvalue=0&svalue='+str(t_int)+';'+str(h_int)+';'+str(HUM_STAT)
        r =  urequests.get(cmd)
        print('DHT data sent')
        time.sleep(10)
        cmd_ground = url_json  + str(ground_idx) + '&nvalue=0&svalue='+ t_ground
        r =  urequests.get(cmd_ground)
        print('DS18x20 data sent')
        time.sleep(10)
        cmd_soil = url_json  + str(soil_idx) + '&nvalue='+ str(SoilMoistVal)+'&svalue=0'
        r =  urequests.get(cmd_soil)
        print('SOIL data sent')
        time.sleep(300)

except Exception as e:
    print('ERROR', e)
    oled.fill(0)
    oled.text('ERROR', 0, 0)
    oled.show()
    time.sleep(2)
    machine.reset()
